#include "../Networking/NetworkingCore.h"

#include <iostream>
#include "Packets.h"

using namespace std;

void SendPacket(NetworkingCore* nc, unsigned int pID);
void CheckPacket(NetworkingCore* nc, unsigned int pID);
void GetLastPacket(NetworkingCore* nc, unsigned int pID);

void CreateRecvThread(NetworkingCore* nc, HANDLE* h);
DWORD WINAPI RecvProc(LPVOID lpParam);

int main()
{
	NetworkingCore* n = new NetworkingCore();

	int check;
	unsigned int packetID;
	bool connected = false;
	string ip;
	string port;

	HANDLE thread;

	while(true)
	{
		cout << "Commands: 1 - Connect | 2 - Send | 3 - Disconnect | 4 - CheckPacket | 5 - GetLastPacket" << endl;
		cout << "Enter a number that corresponds with a command: ";
		cin >> check;
		cout << endl;

		switch(check)
		{
		case 1:
			{
				if(connected)
				{
					cout << "Already connected." << endl;
					break;
				}

				cout << "Enter an IP: ";
				cin >> ip;
				cout << endl;
				cout << "Enter a port: ";
				cin >> port;
				cout << endl;

				if(n->Connect(ip, port))
				{
					cout << "\tConnection successful." << endl;
					connected = true;
					CreateRecvThread(n, &thread);
				}
				else
				{
					cout << "\tConnection unsuccessful." << endl;
					connected = false;
				}
				cout << endl;
				break;
			}
		case 2:
			{
				if(connected)
				{
					cout << "Choose the ID of a dummy packet to send: ";
					cin >> packetID;
					cout << endl;

					SendPacket(n, packetID);
				}
				else
				{
					cout << "Not currently connected." << endl;
				}
				cout << endl;
				break;
			}
		case 3:
			{
				if(connected)
				{
					cout << "Disconnecting..." << endl;
					TerminateThread(thread, 0);
					n->Disconnect();
				}
				else
				{
					cout << "Not currently connected." << endl;
				}
				cout << endl;
				break;
			}
		case 4:
			{
				if(connected)
				{
					cout << "Choose the ID of the packet you wish to check: ";
					cin >> packetID;
					cout << endl;

					CheckPacket(n, packetID);
				}
				else
				{
					cout << "Not currently connected." << endl;
				}
				cout << endl;
				break;
			}
		case 5:
			{
				if(connected)
				{
					cout << "Choose the ID of the packet you wish to check and get a copy of: ";
					cin >> packetID;
					cout << endl;

					GetLastPacket(n, packetID);
				}
				else
				{
					cout << "Not currently connected." << endl;
				}
				cout << endl;
				break;
			}
		default:
			{
				cout << endl;
				break;
			}
		}
	}

	system("pause");
	return 0;
}

void SendPacket(NetworkingCore* nc, unsigned int pID)
{
	switch(pID)
	{
	case 1:
		{
			ClientPacket_JoinServer packet;
			packet.packetID = 1;
			packet.clientID = 1;
			strncpy_s(packet.clientName, "Name1", sizeof(packet.clientName));
			packet.clientName[sizeof(packet.clientName) - 1] = '\0';

			char* sendbuf = static_cast<char*>( static_cast<void*>(&packet) );
			nc->Send(sendbuf, sizeof(packet));

			break;
		}
	case 2:
		{
			ClientPacket_JoinGame packet;
			packet.packetID = 2;
			packet.clientID = 2;
			packet.gameID = 1;

			char* sendbuf = static_cast<char*>( static_cast<void*>(&packet) );
			nc->Send(sendbuf, sizeof(packet));

			break;
		}
	case 3:
		{
			ClientPacket_MoveEvent packet;
			packet.packetID = 0x10 | 0x20 | 0x40 | 0x80;
			packet.packetID = 3;
			packet.clientID = 3;
			packet.gameID = 3;

			char* sendbuf = static_cast<char*>( static_cast<void*>(&packet) );
			nc->Send(sendbuf, sizeof(packet));

			break;
		}
	case 4:
		{
			ClientPacket_FireEvent packet;
			packet.packetID = 4;
			packet.clientID = 4;
			packet.targetID = 0;
			packet.gameID = 4;

			char* sendbuf = static_cast<char*>( static_cast<void*>(&packet) );
			nc->Send(sendbuf, sizeof(packet));

			break;
		}
	case 5:
		{
			ClientPacket_QuitGame packet;
			packet.packetID = 5;
			packet.clientID = 5;
			packet.gameID = 1;

			char* sendbuf = static_cast<char*>( static_cast<void*>(&packet) );
			nc->Send(sendbuf, sizeof(packet));

			break;
		}
	case 6:
		{
			ClientPacket_QuitServer packet;
			packet.packetID = 6;
			packet.clientID = 6;
			packet.gameID = 1;

			char* sendbuf = static_cast<char*>( static_cast<void*>(&packet) );
			nc->Send(sendbuf, sizeof(packet));

			break;
		}
	case 7:
		{
			ServerPacket_SyncLobby packet;
			packet.packetID = 7;
			packet.clientID = 7;
			packet.playersInGame = -32768;
			packet.playersInLobby = 123;

			char* sendbuf = static_cast<char*>( static_cast<void*>(&packet) );
			nc->Send(sendbuf, sizeof(packet));

			break;
		}
	case 8:
		{
			ServerPacket_ClientJoinGame packet;
			packet.packetID = 8;
			packet.clientID = 8;
			packet.gameID = 1;

			char* sendbuf = static_cast<char*>( static_cast<void*>(&packet) );
			nc->Send(sendbuf, sizeof(packet));

			break;
		}
	case 9:
		{
			ServerPacket_SyncGame packet;
			packet.packetID = 9;
			for(int x = 0; x < 8; ++x)
			{
				packet.data[x].clientID = x;
				packet.data[x].health = 100;
				packet.data[x].positionX = 10.0f;
				packet.data[x].positionY = -10.0f;
				packet.data[x].rotation = 3.14159265f;
				packet.data[x].moveFlags = 0x01 | 0x02 | 0x04 | 0x08;
			}

			char* sendbuf = static_cast<char*>( static_cast<void*>(&packet) );
			nc->Send(sendbuf, sizeof(packet));

			break;
		}
	case 10:
		{
			ServerPacket_MoveEvent packet;
			packet.packetID = 0x10 | 0x20 | 0x40 | 0x80;
			packet.packetID = 10;
			packet.clientID = 10;

			char* sendbuf = static_cast<char*>( static_cast<void*>(&packet) );
			nc->Send(sendbuf, sizeof(packet));

			break;
		}
	case 11:
		{
			ServerPacket_FireEvent packet;
			packet.packetID = 11;
			packet.clientID = 11;
			packet.targetID = 12;

			char* sendbuf = static_cast<char*>( static_cast<void*>(&packet) );
			nc->Send(sendbuf, sizeof(packet));

			break;
		}
	case 12:
		{
			ServerPacket_GameOver packet;
			packet.packetID = 12;
			packet.winnerID = 12;
			strncpy_s(packet.winnerName, "Name12", sizeof(packet.winnerName));
			packet.winnerName[sizeof(packet.winnerName) - 1] = '\0';

			char* sendbuf = static_cast<char*>( static_cast<void*>(&packet) );
			nc->Send(sendbuf, sizeof(packet));

			break;
		}
	case 13:
		{
			ServerPacket_ClientQuitGame packet;
			packet.packetID = 13;
			packet.clientID = 13;
			packet.gameID = 1;

			char* sendbuf = static_cast<char*>( static_cast<void*>(&packet) );
			nc->Send(sendbuf, sizeof(packet));

			break;
		}
		break;
	}
}

void CheckPacket(NetworkingCore* nc, unsigned int pID)
{
	if(nc->CheckPacket(pID))
	{
		cout << "Packet of ID '" << pID << "' has recently been updated." << endl;
	}
	else
	{
		cout << "Packet of ID '" << pID << "' is not updated / found." << endl;
	}
}

void GetLastPacket(NetworkingCore* nc, unsigned int pID)
{
	PacketData __pdata = nc->GetLastPacket(pID);

	switch(__pdata.packetID)
	{
	case 0:
		{
			cout << "The packet of ID " << pID << " is not updated / found." << endl;
			break;
		}
	case 1:
		{
			ClientPacket_JoinServer packet = *static_cast<ClientPacket_JoinServer*>( static_cast<void*>(__pdata.data) );

			cout << "Got Echo! Client Join Server!\n";
			cout << "Packet ID: " << (packet.packetID & 0x0F) << endl;
			cout << "Client ID: " << (unsigned int)packet.clientID << endl;
			cout << "Client Name: " << packet.clientName << endl << endl;

			break;
		}
	case 2:
		{
			ClientPacket_JoinGame packet = *static_cast<ClientPacket_JoinGame*>( static_cast<void*>(__pdata.data) );

			cout << "Got Echo! Client Join Game!" << endl;
			cout << "Packet ID: " << (packet.packetID & 0x0F) << endl;
			cout << "Client ID: " << (unsigned int)packet.clientID << endl;
			cout << "Game ID: " << (unsigned int)packet.gameID << endl << endl;
			
			break;
		}
	case 3:
		{
			ClientPacket_MoveEvent packet = *static_cast<ClientPacket_MoveEvent*>( static_cast<void*>(__pdata.data) );

			cout << "Got Echo! Client Move Event!" << endl;
			cout << "Packet ID: " << (packet.packetID & 0x0F) << endl;
			cout << "Client ID: " << (unsigned int)packet.clientID << endl;
			if((((packet.packetID & 0xF0) >> 4) & 0x08) > 0)
			{
				cout << "\tUp ON | ";
			}
			else
			{
				cout << "\tUp OFF | ";
			}
			if((((packet.packetID & 0xF0) >> 4) & 0x04) > 0)
			{
				cout << "Down ON | ";
			}
			else
			{
				cout << "Down OFF | ";
			}
			if((((packet.packetID & 0xF0) >> 4) & 0x02) > 0)
			{
				cout << "Left ON | ";
			}
			else
			{
				cout << "Left OFF | ";
			}
			if((((packet.packetID & 0xF0) >> 4) & 0x01) > 0)
			{
				cout << "Right ON\n";
			}
			else
			{
				cout << "Right OFF\n";
			}
			cout << "Game ID: " << (unsigned int)packet.gameID << endl;
			cout << "\n";

			break;
		}
	case 4:
		{
			ClientPacket_FireEvent packet = *static_cast<ClientPacket_FireEvent*>( static_cast<void*>(__pdata.data) );

			cout << "Got Echo! Client Fire Event!\n" << endl;
			cout << "Packet ID: " << (packet.packetID & 0x0F) << endl;
			cout << "Client ID: " << (unsigned int)packet.clientID << endl;
			cout << "Target ID: " << (unsigned int)packet.targetID << endl;
			cout << "Game ID: " << (unsigned int)packet.gameID << endl;
			cout << endl;

			break;
		}
	case 5:
		{
			ClientPacket_QuitGame packet = *static_cast<ClientPacket_QuitGame*>( static_cast<void*>(__pdata.data) );

			cout << "Got Echo! Client Quit Game!\n" << endl;
			cout << "Packet ID: " << (packet.packetID & 0x0F) << endl;
			cout << "Client ID: " << (unsigned int)packet.clientID << endl;
			cout << "Game ID: " << (unsigned int)packet.gameID << endl;
			cout <<  endl;

			break;
		}
	case 6:
		{
			ClientPacket_QuitServer packet = *static_cast<ClientPacket_QuitServer*>( static_cast<void*>(__pdata.data) );

			cout << "Got Echo! Client Quit Server!\n" << endl;
			cout << "Packet ID: " << (packet.packetID & 0x0F) << endl;
			cout << "Client ID: " << (unsigned int)packet.clientID << endl;
			cout << "Game ID: " << (unsigned int)packet.gameID << endl;
			cout << endl;

			break;
		}
	case 7:
		{
			ServerPacket_SyncLobby packet = *static_cast<ServerPacket_SyncLobby*>( static_cast<void*>(__pdata.data) );

			cout << "Got Echo! Server Sync Lobby!\n" << endl;
			cout << "Packet ID: " << (packet.packetID & 0x0F) << endl;
			cout << "Client ID: " << (unsigned int)packet.clientID << endl;
			cout << "Players in Game 1: " << (packet.playersInGame & 0x0F) << endl;
			cout << "Players in Game 2: " << ((packet.playersInGame >> 4) & 0x0F) << endl;
			cout << "Players in Game 3: " << ((packet.playersInGame >> 8) & 0x0F) << endl;
			cout << "Players in Game 4: " << ((packet.playersInGame >> 12) & 0x0F) << endl;
			cout << "Players in Lobby: " << (unsigned int)packet.playersInLobby << endl;
			cout << endl;

			break;
		}
	case 8:
		{
			ServerPacket_ClientJoinGame packet = *static_cast<ServerPacket_ClientJoinGame*>( static_cast<void*>(__pdata.data) );

			cout << "Got Echo! Server Client Join Game!\n" << endl;
			cout << "Packet ID: " << (packet.packetID & 0x0F) << endl;
			cout << "Client ID: " << (unsigned int)packet.clientID << endl;
			cout << "Game ID: " << (unsigned int)packet.gameID << endl;
			cout << endl;

			break;
		}
	case 9:
		{
			ServerPacket_SyncGame packet = *static_cast<ServerPacket_SyncGame*>( static_cast<void*>(__pdata.data) );

			cout << "Got Echo! Server Sync Game!\n" << endl;
			cout << "Packet ID: " << (packet.packetID & 0x0F) << endl;
			for(PlayerData data : packet.data)
			{
				cout << "Client ID: " << (unsigned int)data.clientID << endl;
				cout << "\tPlayer health: " << (unsigned int)data.health << endl;
				if((data.moveFlags & 0x08) > 0)
				{
					cout << "\tUp ON | ";
				}
				else
				{
					cout << "\tUp OFF | ";
				}
				if((data.moveFlags & 0x04) > 0)
				{
					cout << "Down ON | ";
				}
				else
				{
					cout << "Down OFF | ";
				}
				if((data.moveFlags & 0x02) > 0)
				{
					cout << "Left ON | ";
				}
				else
				{
					cout << "Left OFF | ";
				}
				if((data.moveFlags & 0x01) > 0)
				{
					cout << "Right ON\n";
				}
				else
				{
					cout << "Right OFF\n";
				}
				cout << "\tPosition: X: " << data.positionX;
				cout << "Y: " << data.positionY << endl;
				cout << "\tRotation: " << data.rotation << endl;
				cout << endl;
			}
			cout << endl;

			break;
		}
	case 10:
		{
			ServerPacket_MoveEvent packet = *static_cast<ServerPacket_MoveEvent*>( static_cast<void*>(__pdata.data) );

			cout << "Got Echo! Server Move Event!" << endl;
			cout << "Packet ID: " << (packet.packetID & 0x0F) << endl;
			cout << "Client ID: " << (unsigned int)packet.clientID << endl;
			if((((packet.packetID & 0xF0) >> 4) & 0x08) > 0)
			{
				cout << "\tUp ON | " << endl;
			}
			else
			{
				cout << "\tUp OFF | " << endl;
			}
			if((((packet.packetID & 0xF0) >> 4) & 0x04) > 0)
			{
				cout << "Down ON | " << endl;
			}
			else
			{
				cout << "Down OFF | " << endl;
			}
			if((((packet.packetID & 0xF0) >> 4) & 0x02) > 0)
			{
				cout << "Left ON | " << endl;
			}
			else
			{
				cout << "Left OFF | " << endl;
			}
			if((((packet.packetID & 0xF0) >> 4) & 0x01) > 0)
			{
				cout << "Right ON\n" << endl;
			}
			else
			{
				cout << "Right OFF\n" << endl;
			}
			cout << endl;

			break;
		}
	case 11:
		{
			ServerPacket_FireEvent packet = *static_cast<ServerPacket_FireEvent*>( static_cast<void*>(__pdata.data) );

			cout << "Got Echo! Server Fire Event!" << endl;
			cout << "Packet ID: " << (packet.packetID & 0x0F) << endl;
			cout << "Client ID: " << (unsigned int)packet.clientID << endl;
			cout << "Target ID: " << (unsigned int)packet.targetID << endl;
			cout << endl;

			break;
		}
	case 12:
		{
			ServerPacket_GameOver packet = *static_cast<ServerPacket_GameOver*>( static_cast<void*>(__pdata.data) );

			cout << "Got Echo! Server Game Over!" << endl;
			cout << "Packet ID: " << (packet.packetID & 0x0F) << endl;
			cout << "Winner ID: " << (unsigned int)packet.winnerID << endl;
			cout << "Winner Name: " << packet.winnerName << endl;
			cout << endl;

			break;
		}
	case 13:
		{
			ServerPacket_ClientQuitGame packet = *static_cast<ServerPacket_ClientQuitGame*>( static_cast<void*>(__pdata.data) );

			cout << "Got Echo! Server Client Quit Game!" << endl;
			cout << "Packet ID: " << (packet.packetID & 0x0F) << endl;
			cout << "Client ID: " << (unsigned int)packet.clientID << endl;
			cout << "Game ID: " << (unsigned int)packet.gameID << endl;
			cout << endl;

			break;
		}
	default:
		break;
	}
}

void CreateRecvThread(NetworkingCore* nc, HANDLE* h)
{
	*h = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE) RecvProc, nc, 0, NULL);
}

DWORD WINAPI RecvProc(LPVOID lpParam)
{
	NetworkingCore* nc = static_cast<NetworkingCore*>(lpParam);

	for(;;)
	{
		if(!nc->Receive())
		{
			return FALSE;
		}
	}
}