#ifndef PACKETS_H
#define PACKETS_H

////////////////////////////////////////////////
///
///	WARNING:
///	packetID first 4 bits will be 0-15 and determine the packet type based on the IDs below
///	On MOVE EVENT the last 4 bits will be encoded for the movement flags
///	
///	_ _ _ _ | _ _ _ _
///	^ v < > | _ I D _
///
////////////////////////////////////////////////


struct PlayerData
{
	char clientID;
	char health;
	char moveFlags;
	//extra byte here
	float positionX;
	float positionY;
	float rotation;
};

struct ClientPacket_JoinServer // ID #0
{
	unsigned int packetID;
	char clientID;
	char clientName[30];
};

struct ClientPacket_JoinGame // ID #1
{
	unsigned int packetID;
	char clientID;
	char gameID;
};

struct ClientPacket_MoveEvent // ID #2
{	
	unsigned int packetID;
	char clientID;
	char gameID;
};

struct ClientPacket_FireEvent // ID #3
{
	unsigned int packetID;
	char clientID;
	char targetID; // if 0 no target
	char gameID;
};

struct ClientPacket_QuitGame // ID #4
{
	unsigned int packetID;
	char clientID;
	char gameID;
};

struct ClientPacket_QuitServer // ID #5
{
	unsigned int packetID;
	char clientID;
	char gameID;
};

struct ServerPacket_SyncLobby // ID #6
{
	unsigned int packetID;
	char clientID;		//Gives the client it's ID
	short playersInGame; //first 4 bits is first game, second is second game, etc. BIT_ENCODED
	char playersInLobby; //number of players connected in the lobby. MAX 255
};

struct ServerPacket_ClientJoinGame // ID #7
{
	unsigned int packetID;
	char clientID;
	char gameID;
};

struct ServerPacket_SyncGame // ID #8
{
	unsigned int packetID;
	PlayerData data[8];
};

struct ServerPacket_MoveEvent // ID #9
{
	unsigned int packetID;
	char clientID;
};

struct ServerPacket_FireEvent // ID #10
{
	unsigned int packetID;
	char clientID;
	char targetID; // if 0 no target
};

struct ServerPacket_GameOver // ID #11
{
	unsigned int packetID;
	char winnerID;
	char winnerName[30];
};

struct ServerPacket_ClientQuitGame // ID #12
{
	unsigned int packetID;
	char clientID;
	char gameID;
};

#endif