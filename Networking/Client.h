#pragma once

#define WIN32_LEAN_AND_MEAN

#include <Windows.h>
#include <WinSock2.h>
#include <WS2tcpip.h>
#include <IPHlpApi.h>

#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")
#pragma warning (disable : 4251)

#include <string>
#include <vector>

#include "PacketReporter.h"

class Client
{
private:

	WSAData m_WSAData;
	SOCKET* m_ClientConnection;

protected:

	PacketReporter* reporter;

public:

	Client(PacketReporter* r);
	~Client();

	bool Initialize();

	bool Connect(std::string ipaddr, std::string port);

	bool Send(char* data, int size);

	bool Receive();

	void Disconnect();
};