#pragma once

#include <string>
#include <cstdarg>
#include "NetworkingStructs.h"

/**	Interface for the NetworkingCore
 *	
 *	The interface for the NetworkingCore. This interface is designed to allow access to key functions that an end-user of this core would use in the creation of an online application.
 *	The Core itself handles the receiving of packets using a Reporter system that keeps track of the data for when the user needs it.
 *	Packets MUST use an unsigned int as the packetID and it MUST be the first variable in the packet, I.E. the first 4 bytes.
 */
class _declspec(dllexport) INetworkingCore
{
public:

	/**	
	 *	Connects to a socket using TCP/IP.
	 *	
	 *	std::string ipaddr	-	The IP address of the socket you are trying to connect to.
	 *	std::string port	-	The port of the socket you are trying to connect to.
	 *	
	 *	Returns true if successful, false if not successful.
	 */
	virtual bool Connect(std::string ipaddr, std::string port) = 0;


	/**	
	 *	Sends a chunk of data over the connected socket.
	 *	
	 *	char* data			-	A pointer to the start of the data.
	 *	unsigned int size	-	The size of the data being sent.
	 *	
	 *	Returns true if successful, false if not successful.
	 */
	virtual bool Send(char* data, unsigned int size) = 0;

	virtual bool Receive() = 0;


	/**	
	 *	Disconnects the client from any connection currently active.
	 */
	virtual void Disconnect() = 0;


	/**	
	 *	Checks if a packet has been received after the most recent CheckPacket(ID) call.
	 *	
	 *	unsigned int ID		-	The ID of the packet you are looking to check.
	 *	
	 *	Returns true on the first call if a packet has been received.
	 *	Returns false when called and the packet requested has not changed.
	 */
	virtual bool CheckPacket(unsigned int ID) = 0;


	/**	
	 *	Checks if a packet has been received and updated, then returns a pointer to the packet data.
	 *	
	 *	unsigned int ID		-	The ID of the packet you are looking to check and receive.
	 *
	 *	Returns a char* to the data of the packet requested if it has changed.
	 *	Returns 0 otherwise.
	 */
	virtual PacketData GetLastPacket(unsigned int ID) = 0;
};