#pragma once

#include "INetworkingCore.h"
#include "Client.h"
#include "PacketReporter.h"


class _declspec(dllexport) NetworkingCore : public INetworkingCore
{
private:

	Client* m_Client;
	PacketReporter* m_PacketReporter;

public:

	NetworkingCore();
	~NetworkingCore();

	bool Connect(std::string ipaddr, std::string port) override;

	bool Send(char* data, unsigned int size) override;
	
	bool Receive() override;

	void Disconnect() override;

	bool CheckPacket(unsigned int ID) override;

	PacketData GetLastPacket(unsigned int ID) override;

};